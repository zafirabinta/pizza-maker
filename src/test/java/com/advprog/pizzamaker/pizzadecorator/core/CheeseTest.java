package com.advprog.pizzamaker.pizzadecorator.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CheeseTest {

    private Pizza pizza = new Pizza();

    @Test
    public void testWrapWithCheese() {
        Pizza cheese = new Cheese(pizza);
        assertEquals(cheese.getPepperoni(), pizza.getPepperoni());
        assertEquals(cheese.getMushroom(), pizza.getMushroom());
        assertEquals(cheese.getCheese(), pizza.getCheese() + 1);
    }

}
