package com.advprog.pizzamaker.pizzadecorator.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MushroomTest {

    private Pizza pizza = new Pizza();

    @Test
    public void testWrapWithMushroom() {
        Pizza mushroom = new Mushroom(pizza);
        assertEquals(mushroom.getPepperoni(), pizza.getPepperoni());
        assertEquals(mushroom.getMushroom(), pizza.getMushroom() + 1);
        assertEquals(mushroom.getCheese(), pizza.getCheese());
    }

}
