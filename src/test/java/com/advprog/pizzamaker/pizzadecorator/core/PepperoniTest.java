package com.advprog.pizzamaker.pizzadecorator.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PepperoniTest {

    private Pizza pizza = new Pizza();

    @Test
    public void testWrapWithPepperoni() {
        Pizza pepperoni = new Pepperoni(pizza);
        assertEquals(pepperoni.getPepperoni(), pizza.getPepperoni() + 1);
        assertEquals(pepperoni.getMushroom(), pizza.getMushroom());
        assertEquals(pepperoni.getCheese(), pizza.getCheese());
    }

}
