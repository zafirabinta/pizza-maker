package com.advprog.pizzamaker.pizzadecorator.service;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.advprog.pizzamaker.pizzadecorator.core.PizzaTopping;
import com.advprog.pizzamaker.pizzadecorator.repository.PizzaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PizzaServiceImplTest {

    @InjectMocks
    PizzaServiceImpl pizzaService;

    @Mock
    private PizzaRepository pizzaRepository;

    @Test
    public void whenGetPizzaByIdIsCalledItShouldCallPizzaRepositoryGetNumberOfToppings() {
        try {
            pizzaService.getPizzaById(0);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        verify(pizzaRepository, times(1)).getPepperoniById(0);
        verify(pizzaRepository, times(1)).getMushroomById(0);
        verify(pizzaRepository, times(1)).getCheeseById(0);
    }

    @Test
    public void whenGetPizzaByIdIsCalledOutOfBoundItShouldThrowException() {
        try {
            pizzaService.getPizzaById(Integer.MAX_VALUE);
            assert (false);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Test
    public void whenGetNewIdIsCalledItShouldCallPizzaRepositoryGetNumberOfPizza() {
        pizzaService.getNewId();

        verify(pizzaRepository, times(1)).getNumberofPizza();
        lenient().when(pizzaService.getNewId()).thenReturn(0);
    }

    @Test
    public void whenAddToppingIsCalledItShouldCallPizzaRepositorySaveToDB() {
        int oldPepperoni = pizzaRepository.getPepperoniById(0);

        PizzaTopping pizzaTopping = new PizzaTopping(0, "pepperoni");
        pizzaService.addTopping(pizzaTopping);

        verify(pizzaRepository, times(1)).save(pizzaTopping);
        lenient().when(pizzaRepository.getPepperoniById(0)).thenReturn(oldPepperoni + 1);
    }

}
