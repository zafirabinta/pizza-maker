package com.advprog.pizzamaker.level.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.advprog.pizzamaker.level.service.LevelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = LevelRestController.class)
@AutoConfigureMockMvc
public class LevelRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LevelService levelService;

    @Test
    @WithMockUser
    public void whenOrderURLIsAccessedItShouldCallLevelServiceRequestOrder() throws Exception {

        mockMvc.perform(get("/order"))
            .andExpect(handler().methodName("requestOrder"));
    }

    @Test
    @WithMockUser
    public void whenGetLevelURLIsAccessedItShouldCallLevelServiceGetLevel() throws Exception {

        mockMvc.perform(get("/get-level"))
            .andExpect(handler().methodName("getLevel"));
    }

    @Test
    @WithMockUser
    public void whenRetryLevelURLIsAccessedItShouldCallLevelServiceRetryLevel() throws Exception {

        mockMvc.perform(get("/retry-level"))
            .andExpect(handler().methodName("retryLevel"));
    }
}