package com.advprog.pizzamaker.level.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.CreatePizzaButton;
import com.advprog.pizzamaker.level.core.buttons.SubmitButton;
import com.advprog.pizzamaker.level.core.buttons.TrashButton;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Collection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ButtonRepositoryTest {
    private ButtonRepository buttonRepo;
    private Level level;

    @BeforeEach
    public void setUp() {
        buttonRepo = new ButtonRepository();
        level = new Level();
    }

    @Test
    public void testAddButtonShouldAddItToTheRepository() {
        Button newButton = new CreatePizzaButton(level);

        buttonRepo.addButton(newButton);
        Collection<Button> repositoryButton = buttonRepo.getButtons();

        assertThat(repositoryButton).hasSize(1);
        assertThat(repositoryButton).contains(newButton);
    }

    @Test
    public void testExecuteButtonShouldBeExecuteFromRepository() {
        CreatePizzaButton mockedButton = mock(CreatePizzaButton.class);

        buttonRepo.addButton(mockedButton);
        buttonRepo.executeButton(mockedButton.getName());

        verify(mockedButton).execute();
    }

    @Test
    public void testSubmitShouldBeSubmitFromRepository() throws JsonProcessingException {
        SubmitButton mockedButton = new SubmitButton(level);
        level.createPizza();
        level.createOrder();

        buttonRepo.addButton(mockedButton);
        buttonRepo.submit(10);

        // need to verify
    }

    @Test
    public void testTrashButtonShouldBePenaltyFromRepository() {
        TrashButton mockedButton = new TrashButton(level);
        level.createPizza();

        buttonRepo.addButton(mockedButton);
        buttonRepo.trash();

        // need to verify
    }
}