package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.CreatePizzaButton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreatePizzaButtonTest {

    private Button createPizzaButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        createPizzaButton = new CreatePizzaButton(level);
    }

    @Test
    public void testCreatePizzaButtonName() {
        assertEquals("create pizza", createPizzaButton.getName());
    }

    @Test
    public void testCreatePizzaButtonMakePizza() {
        createPizzaButton.execute();
        assertNotNull(level.getPizza());
    }

    @Test
    public void testSubmitNotWorking() {
        assertEquals(0, createPizzaButton.submit(10));
    }

    @Test
    public void testPenaltyNotWorking() {
        assertEquals(0, createPizzaButton.penalty());
    }
}