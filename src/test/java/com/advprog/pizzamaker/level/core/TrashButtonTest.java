package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.TrashButton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrashButtonTest {

    private Button trashButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        trashButton = new TrashButton(level);
    }

    @Test
    public void testTrashButtonName() {
        assertEquals("trash", trashButton.getName());
    }

    @Test
    public void testTrashPizzaPenalty() {
        level.createPizza();
        int penalty = trashButton.penalty();
        assertEquals(50, penalty);
        assertEquals(-50, level.getScore());
    }

    @Test
    public void testExecuteNotWorking() {
        trashButton.execute();
    }

    @Test
    public void testSubmitNotWorking() {
        assertEquals(0, trashButton.submit(10));
    }
}