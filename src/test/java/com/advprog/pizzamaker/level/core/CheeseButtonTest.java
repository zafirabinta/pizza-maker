package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.CheeseButton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CheeseButtonTest {

    private Button cheeseButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        cheeseButton = new CheeseButton(level);
    }

    @Test
    public void testCheeseButtonName() {
        assertEquals("cheese", cheeseButton.getName());
    }

    @Test
    public void testAddCheesetoPizza() {
        level.createPizza();
        cheeseButton.execute();
        assertEquals(1, level.getPizza().getCheese());
    }

    @Test
    public void testSubmitNotWorking() {
        assertEquals(0, cheeseButton.submit(10));
    }

    @Test
    public void testPenaltyNotWorking() {
        assertEquals(0, cheeseButton.penalty());
    }
}