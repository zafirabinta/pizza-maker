package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderTest {

    private Order order;
    private List<String> toppings;

    /**
     * Set up before each Order Test.
     */
    @BeforeEach
    public void setUp() {
        toppings = Arrays.asList("pepperoni", "cheese", "mushroom");
        order = new Order(toppings);
        order.makeOrder();
    }

    @Test
    public void testOrderMakerandGetTotal() {
        int total = 0;
        for (int ii = 0; ii < toppings.size(); ii++) {
            int i = 1;
            while (i <= 8) {
                try {
                    boolean response = order.getTopping(toppings.get(ii), i);
                    if (response) {
                        total += i;
                        break;
                    }
                    i++;
                } catch (Exception e) {
                    break;
                }
            }
        }
        assertEquals(8, total);
    }

    @Test
    public void testOrderNotGetCorrectTopping() {
        assertEquals(false, order.getTopping("pineapple", 100));
    }
}