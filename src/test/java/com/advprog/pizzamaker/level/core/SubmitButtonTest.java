package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.SubmitButton;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SubmitButtonTest {

    private Button submitButton;
    private Level level;

    @BeforeEach
    public void setUp() throws Exception {
        level = new Level();
        submitButton = new SubmitButton(level);
    }

    @Test
    public void testSubmitButtonName() {
        assertEquals("submit", submitButton.getName());
    }

    @Test
    public void testSubmittingPizzaFailed() throws JsonProcessingException {
        level.createPizza();
        level.createOrder();
        int score = submitButton.submit(10);
        assertEquals(-50, score);
        assertEquals(-50, level.getScore());
    }

    @Test
    public void testSubmittingPizzaSucceed() throws JsonProcessingException {
        level.createPizza();
        level.createFakeOrder();
        AddOn testAddOn = level.getAddOn();
        testAddOn.addMushroom();
        testAddOn.addPepperoni();
        testAddOn.addCheese();
        int score = submitButton.submit(10);
        assertTrue(score > 0);
    }

    @Test
    public void testExecuteNotWorking() {
        submitButton.execute();
    }

    @Test
    public void testPenaltyNotWorking() {
        assertEquals(0, submitButton.penalty());
    }
}