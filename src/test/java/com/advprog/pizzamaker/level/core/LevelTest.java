package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LevelTest {
    private Level level;

    @BeforeEach
    public void setUp() {
        level = new Level();
    }

    @Test
    public void testSucceedNextLevel() {
        int pastLvl = level.getLevel();
        level.changeScore(3000);
        int response = level.nextLevel();
        int nextLvl = level.getLevel();
        assertEquals(nextLvl - 1, pastLvl);
        assertEquals(1, response);
    }

    @Test
    public void testFailedNextLevel() {
        int pastLvl = level.getLevel();
        int response = level.nextLevel();
        int nextLvl = level.getLevel();
        assertEquals(nextLvl, pastLvl);
        assertEquals(-1, response);
    }

    @Test
    public void testRetryLevel() {
        level.retryLevel();
        assertEquals(0, level.getScore());
    }
}