package com.advprog.pizzamaker.level.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

public class ButtonTest {
    private Class<?> buttonClass;

    @Mock
    private Level level;

    @BeforeEach
    public void setup() throws Exception {
        buttonClass = Class.forName("com.advprog.pizzamaker.level.core.buttons.Button");
    }

    @Test
    public void testButtonisAPublicAbstractClass() {
        int classModifiers = buttonClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testButtonHasExecuteAbstractMethod() throws Exception {
        Method cast = buttonClass.getDeclaredMethod("execute");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }

    @Test
    public void testButtonHasPenaltyAbstractMethod() throws Exception {
        Method cast = buttonClass.getDeclaredMethod("penalty");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }

    @Test
    public void testButtonHasGetNameAbstractMethod() throws Exception {
        Method cast = buttonClass.getDeclaredMethod("getName");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }

}