package com.advprog.pizzamaker.scoreboard.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.advprog.pizzamaker.scoreboard.service.ScoreboardServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = TopScoreController.class)

@AutoConfigureMockMvc
public class TopScoreControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScoreboardServiceImp scoreboardServiceImp;

    /**
     * Setting Up for handling exceptions.
     */
    @Before
    public void setUp() {
        final TopScoreController controller = new TopScoreController(scoreboardServiceImp);

        mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setViewResolvers(new StandaloneMvcTestViewResolver())
                        .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                        .build();
    }

    @Test
    @WithMockUser
    public void testStatusOK() throws Exception {

        mockMvc.perform(get("/topScore"))
                .andExpect(status().isOk());
    }

}
