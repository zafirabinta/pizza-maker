package com.advprog.pizzamaker.scoreboard.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.repository.PlayerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ScoreboardServiceImpTest {

    @Mock
    private PlayerRepository repository;

    @InjectMocks
    private ScoreboardServiceImp service;

    private Player player;

    @Test
    public void getPlayersTest() {
        player = new Player();
        player.setUsername("username");
        player.setId("1212121");
        player.setEmail("aan@gmail.com");
        player.setScore(9100);

        service.addPlayer(player);
        service.getPlayers();

        verify(repository, times(1)).save(player);
        verify(repository, times(4)).findAll();
    }

    @Test
    public void whenServicefindAllItShouldCallfindAllRepository() {
        service.findAll();
        verify(repository, times(1)).findAll();
    }

    @Test
    public void whenServiceFindByEmailItShouldCallFindByEmailRepository() {
        player = new Player();
        player.setUsername("username");
        player.setId("1212121");
        player.setEmail("aan@gmail.com");
        player.setScore(9100);
        service.addPlayer(player);
        service.findByEmail("aan@gmail.com");

        verify(repository, times(1)).findByEmail("aan@gmail.com");
    }

    @Test
    public void whenUpdateScoreLevelItShouldReturnPlayer() {
        player = new Player();
        String email = "aan@gmail.com";
        player.setUsername("username");
        player.setId("1212121");
        player.setEmail(email);
        player.setScore(9100);

        service.addPlayer(player);
        service.updatePlayer();
        service.updateScoreLevel(email, 900, 10);

        verify(repository, times(6)).findAll();
        verify(repository, times(1)).save(player);

        assertTrue(service.getRepository() != null);

    }
}
