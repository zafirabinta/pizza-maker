package com.advprog.pizzamaker.scoreboard.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerTest {
    private Player player;
    private TopScore topScore = new TopScore();

    @BeforeEach
    public void setUp() throws Exception {
        player = new Player();
        player.setTopScore(topScore);
    }

    @Test
    public void setNameShouldChangePlayerName() {
        player.setUsername("Vincent");
        assertTrue(player.getUsername().equals("Vincent"));
    }

    @Test
    public void getNameShouldReturnString() {
        assertTrue(player.getUsername() instanceof String);
    }

    @Test
    public void setEmailShouldChangeEmail() {
        player.setEmail("aan@gmail.com");
        assertTrue(player.getEmail().equals("aan@gmail.com"));
    }

    @Test
    public void updateScoreShouldChangePlayerScore() {
        player.increaseScore(500);
        assertEquals(500, player.getScore());
    }

    @Test
    public void setIdShouldChangeId() {
        player.setId("1256152761");
        assertTrue(player.getId().equals("1256152761"));
        assertTrue(player.getId() instanceof String);
    }

    @Test
    public void setLevelShouldChangeLevel() {
        player.setLevel(2);
        assertEquals(player.getLevel(), 2);
    }

    @Test
    public void getTopScoreShouldReturnTopScore() {
        assertTrue(player.getTopScore() instanceof TopScore);
    }

    @Test
    public void setScoreShouldChangePlayerScore() {
        player.setScore(800);
        assertEquals(800, player.getScore());
    }

    @Test
    public void setRankShouldChangePlayerRank() {
        player.setRank(2);
        assertEquals(2, player.getRank());
    }

}
