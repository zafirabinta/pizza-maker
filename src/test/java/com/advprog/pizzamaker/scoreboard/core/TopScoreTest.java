package com.advprog.pizzamaker.scoreboard.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.core.TopScore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TopScoreTest {
    TopScore topScore;

    @BeforeEach
    public void setUp() {
        topScore = new TopScore();
    }

    @Test
    public void testWhenAddTopScoreItWillDescendingOrder() {
        Player player1 = new Player();
        player1.setTopScore(topScore);
        player1.setUsername("Guest1");
        player1.setEmail("guest1@gmail.com");
        Player player2 = new Player();
        player2.setTopScore(topScore);
        player2.setUsername("Guest2");
        player2.setEmail("guest2@gmail.com");
        Player player3 = new Player();
        player3.setTopScore(topScore);
        player3.setUsername("Guest3");
        player3.setEmail("guest3@gmail.com");
        player1.processScore(17871);
        player3.processScore(90909);
        player2.processScore(15211);
        Player player4 = new Player();
        player4.setTopScore(topScore);
        player4.setUsername("Guest4");
        player4.setEmail("guest4@gmail.com");
        player4.processScore(999);

        assertEquals(player3.getScore(), topScore.listOfScore.get(0).getScore());
        assertEquals(player4.getRank(), 4);
        assertTrue(player1 instanceof Player);
    }

    @Test
    public void testRewritePlayer() {
        Player player = new Player();
        player.setTopScore(topScore);
        player.setEmail("newPlayer@gmail.com");
        Player newPlayer = player;
        newPlayer.setScore(9000);
        topScore.rewritePlayer(newPlayer);
        Player player1 = new Player();
        assertEquals(newPlayer.getEmail(), topScore.listOfScore.get(0).getEmail());
        assertTrue(topScore.containsEmail(newPlayer));
        assertEquals(topScore.listOfScore.indexOf(player1), -1);
    }
}
