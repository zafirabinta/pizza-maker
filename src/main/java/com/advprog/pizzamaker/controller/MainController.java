package com.advprog.pizzamaker.controller;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.service.ScoreboardService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MainController {

    @Autowired
    ScoreboardService scoreboardService;

    public MainController(ScoreboardService scoreboardService) {
        this.scoreboardService = scoreboardService;
    }

    /**
     * checking UI for navigation bar.
     *
     * @return navbar.html
     */
    @RequestMapping(path = "/navbar")
    @ResponseBody
    private String helloWorld1() {
        return "navbar";
    }

    /**
     * homepage.
     *
     * @return homepage.html
     */
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String helloWorld(Model model, @AuthenticationPrincipal OAuth2User user) {
        if (user != null) {
            if (!(scoreboardService.findByEmail(user.getAttribute("email")).isPresent())) {
                Player player = new Player();
                player.setEmail(user.getAttribute("email"));
                player.setUsername(user.getAttribute("name"));
                player.setId(user.getAttribute("sub"));
                scoreboardService.addPlayer(player);

                List<Player> userScore = scoreboardService.getPlayers();
                model.addAttribute("userScore", userScore);
                Player player1 = scoreboardService.findByEmail(user.getAttribute("email")).get();
                model.addAttribute("player", player1);
            } else {
                List<Player> userScore = scoreboardService.getPlayers();
                model.addAttribute("userScore", userScore);
                Player player = scoreboardService.findByEmail(user.getAttribute("email")).get();
                model.addAttribute("player", player);
            }
        }
        return "homepage";
    }
}
