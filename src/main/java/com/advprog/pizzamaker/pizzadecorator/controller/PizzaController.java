package com.advprog.pizzamaker.pizzadecorator.controller;

import com.advprog.pizzamaker.pizzadecorator.core.Pizza;
import com.advprog.pizzamaker.pizzadecorator.core.PizzaTopping;
import com.advprog.pizzamaker.pizzadecorator.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/pizza")
public class PizzaController {

    @Autowired
    PizzaService pizzaService;

    /**
     * Display Pizza with new id.
     *
     * @param redirectAttributes redirect attribute: new id.
     * @return redirect to display Pizza with new id.
     */
    @GetMapping()
    public String pizza(RedirectAttributes redirectAttributes) {
        int newId = pizzaService.getNewId();
        redirectAttributes.addAttribute("id", newId);
        return "redirect:/pizza/{id}";
    }

    /**
     * Display Pizza with specified id.
     *
     * @param model number of pepperoni, mushroom, and cheese.
     * @param id    id of Pizza to be displayed.
     * @return display Pizza with specified id.
     */
    @GetMapping("/{id}")
    public String pizzaWithId(Model model, @PathVariable int id) {
        try {
            Pizza pizza = pizzaService.getPizzaById(id);
            model.addAttribute("pepperoni", pizza.getPepperoni());
            model.addAttribute("mushroom", pizza.getMushroom());
            model.addAttribute("cheese", pizza.getCheese());
            return "pizza";
        } catch (Exception exception) {
            return "pizza-exceeded";
        }
    }

    @PostMapping
    public void addTopping(@RequestBody PizzaTopping pizzaTopping) {
        pizzaService.addTopping(pizzaTopping);
    }

}
