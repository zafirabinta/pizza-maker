package com.advprog.pizzamaker.pizzadecorator.core;

public enum AddPizzaDecorator {
    PEPPERONI,
    MUSHROOM,
    CHEESE;

    /**
     * Add Pizza Decorator to Pizza.
     *
     * @param pizza Pizza that need to be decorated.
     * @return decorated Pizza.
     */
    public Pizza add(Pizza pizza) {

        if (this == AddPizzaDecorator.PEPPERONI) {
            pizza = new Pepperoni(pizza);
        } else if (this == AddPizzaDecorator.MUSHROOM) {
            pizza = new Mushroom(pizza);
        } else if (this == AddPizzaDecorator.CHEESE) {
            pizza = new Cheese(pizza);
        }

        return pizza;

    }
}
