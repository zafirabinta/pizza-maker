package com.advprog.pizzamaker.pizzadecorator.core;

public class Mushroom extends PizzaDecorator {

    public Mushroom(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public int getMushroom() {
        return this.pizza.getMushroom() + 1;
    }

}
