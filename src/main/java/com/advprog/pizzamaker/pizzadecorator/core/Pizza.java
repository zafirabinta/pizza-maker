package com.advprog.pizzamaker.pizzadecorator.core;

import org.springframework.stereotype.Component;

@Component
public class Pizza {
    public Pizza() {
    }

    public int getPepperoni() {
        return 0;
    }

    public int getMushroom() {
        return 0;
    }

    public int getCheese() {
        return 0;
    }

}
