package com.advprog.pizzamaker.pizzadecorator.core;

public class Cheese extends PizzaDecorator {

    public Cheese(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public int getCheese() {
        return this.pizza.getCheese() + 1;
    }

}
