package com.advprog.pizzamaker.pizzadecorator.repository;

import com.advprog.pizzamaker.pizzadecorator.core.PizzaTopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PizzaRepository extends JpaRepository<PizzaTopping, Integer> {

    @Query("SELECT COUNT (distinct topping.pizzaId) FROM PizzaTopping topping")
    int getNumberofPizza();

    @Query("SELECT COUNT(topping) FROM PizzaTopping topping "
        + "WHERE topping.pizzaId = ?1 AND topping.toppingType = 'pepperoni'")
    int getPepperoniById(int id);

    @Query("SELECT COUNT(topping) FROM PizzaTopping topping "
        + "WHERE topping.pizzaId = ?1 AND topping.toppingType = 'mushroom'")
    int getMushroomById(int id);

    @Query("SELECT COUNT(topping) FROM PizzaTopping topping "
        + "WHERE topping.pizzaId = ?1 AND topping.toppingType = 'cheese'")
    int getCheeseById(int id);

}
