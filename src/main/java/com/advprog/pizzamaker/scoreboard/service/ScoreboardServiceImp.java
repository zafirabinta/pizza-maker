package com.advprog.pizzamaker.scoreboard.service;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.core.TopScore;
import com.advprog.pizzamaker.scoreboard.repository.PlayerRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScoreboardServiceImp implements ScoreboardService {
    private TopScore topScore = new TopScore();

    private final PlayerRepository repository;

    public ScoreboardServiceImp(PlayerRepository playerRepository) {
        repository = playerRepository;
    }

    @Override
    public List<Player> getPlayers() {
        List<Player> players = new ArrayList<>();
        updatePlayer();
        int sumOfPlayers = topScore.listOfScore.size();
        if (sumOfPlayers > 10) {
            sumOfPlayers = 10;
        }

        if (sumOfPlayers == 0) {
            return players;
        }

        for (int i = 0; i < sumOfPlayers; i++) {
            players.add(topScore.listOfScore.get(i));
        }

        return players;
    }

    @Override
    public PlayerRepository getRepository() {
        return this.repository;
    }

    @Override
    public List<Player> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Player> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public void addPlayer(Player player) {
        repository.save(player);
        player.setTopScore(topScore);
        updatePlayer();
    }

    @Override
    public void updatePlayer() {
        if (repository.findAll() != null) {
            for (Player player : repository.findAll()) {
                player.setTopScore(topScore);
                repository.save(player);
            }
        }
    }

    @Override
    public Player updateScoreLevel(String email, int score, int level) {
        updatePlayer();
        if (repository.findByEmail(email).isPresent()) {
            repository.findByEmail(email).get().processScore(score);
            repository.findByEmail(email).get().setLevel(level);
            topScore.rewritePlayer(repository.findByEmail(email).get());
            updatePlayer();
            return repository.findByEmail(email).get();
        }

        return null;
    }

}
