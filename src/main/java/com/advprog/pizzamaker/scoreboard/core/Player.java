package com.advprog.pizzamaker.scoreboard.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "player")
public class Player {

    @Id
    @Column(name = "Id")
    private String id;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "rank")
    private int rank;

    @Column(name = "score")
    private int score;

    @Column(name = "level")
    private int level;

    @Transient
    private TopScore topScore;

    /**
     * Default Constructor.
     * Constructor DataPlayer need to saving TopScore.
     */
    public Player() {
        this.email = "guest@gmail.com";
        this.username = "guest";
        this.score = 0;
        this.level = 1;
    }

    public void setTopScore(TopScore topScore) {
        this.topScore = topScore;
        this.topScore.addTopScore(this);
    }

    public TopScore getTopScore() {
        return this.topScore;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUsername() {
        return this.username;
    }

    public String getEmail() {
        return this.email;
    }

    public int getRank() {
        return this.rank;
    }

    public int getScore() {
        return this.score;
    }

    public void update() {
        int index = topScore.indexOf(this);
        setRank(index + 1);
    }

    public void processScore(int score) {
        increaseScore(score);
        topScore.broadcast();
    }

    protected void increaseScore(int score) {
        this.score += score;
    }
}
