package com.advprog.pizzamaker.scoreboard.controller;

import com.advprog.pizzamaker.scoreboard.core.Player;
import com.advprog.pizzamaker.scoreboard.service.ScoreboardService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/topScore")
public class TopScoreController {

    @Autowired
    ScoreboardService scoreboardService;

    public TopScoreController(ScoreboardService scoreboardService) {
        this.scoreboardService = scoreboardService;
    }

    /**
     * Top Score page controller.
     * @return topScore.html
     */
    @GetMapping()
    public String topScore(Model model, @AuthenticationPrincipal OAuth2User user) {
        List<Player> topScorer = scoreboardService.getPlayers();
        model.addAttribute("topScorer", topScorer);
        if (user != null) {
            scoreboardService.updatePlayer();
            Player player = scoreboardService.findByEmail(user.getAttribute("email")).get();
            model.addAttribute("player", player);
        }
        return "topScore";
    }
}
