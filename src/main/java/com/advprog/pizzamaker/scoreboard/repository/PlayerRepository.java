package com.advprog.pizzamaker.scoreboard.repository;

import com.advprog.pizzamaker.scoreboard.core.Player;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
    List<Player> findAll();

    Optional<Player> findByEmail(String email);
}
