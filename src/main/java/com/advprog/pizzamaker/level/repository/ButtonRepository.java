package com.advprog.pizzamaker.level.repository;

import com.advprog.pizzamaker.level.core.buttons.Button;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class ButtonRepository {
    private Map<String, Button> buttons;

    public ButtonRepository() {
        buttons = new HashMap<>();
    }

    public void addButton(Button button) {
        this.buttons.put(button.getName(), button);
    }

    public void executeButton(String buttonType) {
        this.buttons.get(buttonType).execute();
    }

    public int submit(int time) {
        return this.buttons.get("submit").submit(time);
    }

    public int trash() {
        return this.buttons.get("trash").penalty();
    }

    public Collection<Button> getButtons() {
        return this.buttons.values();
    }
}