package com.advprog.pizzamaker.level.controller;

import com.advprog.pizzamaker.level.service.LevelService;
import com.advprog.pizzamaker.scoreboard.service.ScoreboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class LevelController {

    @Autowired
    private LevelService levelService;

    @Autowired
    private ScoreboardService scoreboardService;

    public LevelController(ScoreboardService scoreboardService, LevelService levelService) {
        this.scoreboardService = scoreboardService;
        this.levelService = levelService;
    }

    /**
     * Level page to play the game. 
     */
    @RequestMapping(value = "/level", method = RequestMethod.GET) 
    public String level(Model model) {
        model.addAttribute("level", levelService.getLevelDifficulty());
        model.addAttribute("buttons", levelService.getButtons());
        return "level";
    }

    @RequestMapping(value = "/make-pizza", method = RequestMethod.GET)
    @ResponseBody
    public String makePizza() {
        levelService.editPizza("create pizza");
        return "MADE!";
    }

    /**
     * Adding toppings by parameter of it's topping.
     */
    @RequestMapping(value = "/add-topping", method = RequestMethod.POST)
    @ResponseBody
    public String addTopping(@RequestBody String toppingPizza) {
        toppingPizza = toppingPizza.substring(0, toppingPizza.length() - 1);
        levelService.editPizza(toppingPizza);
        return toppingPizza;
    }

    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    @ResponseBody
    public int submit(@RequestBody String time) {
        time = time.replaceAll("\\D+","");
        return levelService.submit(Integer.parseInt(time));
    }

    /**
     * Delete pizza and give penalty.
     */
    @RequestMapping(value = "/trash", method = RequestMethod.GET)
    @ResponseBody
    public int trash() {
        return levelService.trash();
    }

    /**
     * Get another level with more difficulties.
     */
    @RequestMapping(value = "/next-level", method = RequestMethod.GET)
    @ResponseBody
    public int nextLevel(@AuthenticationPrincipal OAuth2User user) {
        if (levelService.getLevel() != null) {
            int score = levelService.getLevel().getScore();
            int level = levelService.getLevel().getLevel() + 1;
            scoreboardService.updateScoreLevel(user.getAttribute("email"), score, level);
        }
        return levelService.nextLevel();
    }

    @RequestMapping(value = "/remove-order", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeOrder() {
        levelService.removeOrder();
    }
}