package com.advprog.pizzamaker.level.controller;

import com.advprog.pizzamaker.level.core.Order;
import com.advprog.pizzamaker.level.service.LevelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LevelRestController {

    @Autowired
    private LevelService levelService;

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String requestOrder() throws JsonProcessingException {
        return levelService.createOrder();
    }

    @RequestMapping(value = "/get-level", method = RequestMethod.GET)
    public int getLevel() {
        return levelService.getLevelDifficulty();
    }

    @RequestMapping(value = "/retry-level", method = RequestMethod.GET)
    public void retryLevel() {
        levelService.retryLevel();
    }
}