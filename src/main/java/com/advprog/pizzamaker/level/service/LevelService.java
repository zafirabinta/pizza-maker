package com.advprog.pizzamaker.level.service;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.Order;
import com.advprog.pizzamaker.level.core.buttons.Button;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface LevelService {
    public int nextLevel();

    public void retryLevel();

    public void editPizza(String toppingPizza);

    public int submit(int time);

    public int trash();

    public Iterable<Button> getButtons();
    
    public String createOrder() throws JsonProcessingException;

    public int getLevelDifficulty();

    public void removeOrder();

    public Level getLevel();
}