package com.advprog.pizzamaker.level.service;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.Order;
import com.advprog.pizzamaker.level.core.buttons.Button;
import com.advprog.pizzamaker.level.core.buttons.CheeseButton;
import com.advprog.pizzamaker.level.core.buttons.CreatePizzaButton;
import com.advprog.pizzamaker.level.core.buttons.MushroomButton;
import com.advprog.pizzamaker.level.core.buttons.PepperoniButton;
import com.advprog.pizzamaker.level.core.buttons.SubmitButton;
import com.advprog.pizzamaker.level.core.buttons.TrashButton;
import com.advprog.pizzamaker.level.repository.ButtonRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LevelServiceImpl implements LevelService {
    private Level level;
    private ButtonRepository buttonRepo;

    /**
     * Make new level, set seeds, and make those buttons works.
     */
    public LevelServiceImpl(ButtonRepository buttonRepo) {
        this.buttonRepo = buttonRepo;
        this.level = new Level();
        this.seed();
    }

    @Override
    public int nextLevel() {
        return this.level.nextLevel();
    }

    @Override
    public void retryLevel() {
        this.level.retryLevel();
    }

    @Override
    public void editPizza(String toppingPizza) {
        this.buttonRepo.executeButton(toppingPizza);
    }

    @Override
    public int submit(int time) {
        return this.buttonRepo.submit(time);
    }

    @Override
    public int trash() {
        return this.buttonRepo.trash();
    }

    /**
     * To put buttons in level.
     */
    public void seed() {
        this.buttonRepo.addButton(new CreatePizzaButton(level));
        this.buttonRepo.addButton(new CheeseButton(level));
        this.buttonRepo.addButton(new MushroomButton(level));
        this.buttonRepo.addButton(new PepperoniButton(level));
        this.buttonRepo.addButton(new SubmitButton(level));
        this.buttonRepo.addButton(new TrashButton(level));
    }

    public Iterable<Button> getButtons() {
        return buttonRepo.getButtons();
    }

    public String createOrder() throws JsonProcessingException {
        return level.createOrder();
    }

    @Override
    public Level getLevel() {
        return this.level;
    }

    public int getLevelDifficulty() {
        return this.level.getLevel();
    }

    public void removeOrder() {
        this.level.removeOrder();
    }
}