package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public abstract class Button {
    public Level level;

    public Button(Level level) {
        this.level = level;
    }

    public abstract void execute();

    public abstract int penalty();

    public abstract int submit(int time);

    public abstract String getName();
}