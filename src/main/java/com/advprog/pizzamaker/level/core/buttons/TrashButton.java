package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.pizzadecorator.core.Pizza;

public class TrashButton extends Button {
    private String name = "trash";

    public TrashButton(Level level) {
        super(level);
    }

    @Override
    public int penalty() {
        Pizza pizza = this.level.getPizza();
        // total toppings on the pizza as total
        // int penalty = 50 + topping * 5
        // temp
        int penalty = 50;
        this.level.penalty(penalty);
        return penalty;
    }

    public void execute() {
        // do nothing
    }

    public int submit(int time) {
        return 0;
    }

    public String getName() {
        return this.name;
    }
}