package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public abstract class ToppingButton extends Button {

    public ToppingButton(Level level) {
        super(level);
    }

    public abstract void execute();

    public abstract String getName();
}