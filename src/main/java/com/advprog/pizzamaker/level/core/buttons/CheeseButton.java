package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public class CheeseButton extends ToppingButton {
    private String name = "cheese";

    public CheeseButton(Level level) {
        super(level);
    }

    @Override
    public void execute() {
        this.level.getAddOn().addCheese();
    }

    public String getName() {
        return this.name;
    }

    public int submit(int time) {
        return 0;
    }

    public int penalty() {
        return 0;
    }
}