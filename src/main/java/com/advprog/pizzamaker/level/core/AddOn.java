package com.advprog.pizzamaker.level.core;

import com.advprog.pizzamaker.pizzadecorator.core.Cheese;
import com.advprog.pizzamaker.pizzadecorator.core.Mushroom;
import com.advprog.pizzamaker.pizzadecorator.core.Pepperoni;
import com.advprog.pizzamaker.pizzadecorator.core.Pizza;

public class AddOn {
    private Pizza pizza;

    public AddOn() {
        this.pizza = new Pizza();
    }

    public Pizza getPizza() {
        return this.pizza;
    }

    public void addMushroom() {
        this.pizza = new Mushroom(this.pizza);
    }

    public void addPepperoni() {
        this.pizza = new Pepperoni(this.pizza);
    }

    public void addCheese() {
        this.pizza = new Cheese(this.pizza);
    }
}