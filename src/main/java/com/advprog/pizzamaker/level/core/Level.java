package com.advprog.pizzamaker.level.core;

import com.advprog.pizzamaker.pizzadecorator.core.Pizza;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Level {
    final List<String> toppings = Arrays.asList("cheese", "mushroom", "pepperoni");
    private List<Order> orderList;
    private int level;
    private int finalScore = 0;
    private AddOn addOn;

    public Level() {
        this.level = 1;
        this.orderList = new ArrayList<>();
    }

    public void createPizza() {
        this.addOn = new AddOn();
    }

    public Pizza getPizza() {
        return this.addOn.getPizza();
    }

    public AddOn getAddOn() {
        return this.addOn;
    }

    public void increaseScore(int score) {
        this.finalScore += score;
    }

    public void penalty(int penalty) {
        this.finalScore -= penalty;
        this.addOn = null;
    }

    public int getLevel() {
        return this.level;
    }

    /**
     * Get First Order as submitted pizza should match first order.
     */
    public Order getFirstOrder() {
        if (this.orderList.size() > 0) {
            return this.orderList.get(0);
        }
        return null;
    }

    /**
     * Checking if score is enough to go to harder lvl.
     */
    public int nextLevel() {
        if (this.finalScore >= (1000 * level)) {
            this.level += 1;
            this.orderList = new ArrayList<>();
            this.finalScore = 0;
            return 1;
        }

        return -1;
    }

    /** 
     * Making order to get score when pizza ingredients match.
    */
    public String createOrder() throws JsonProcessingException {
        Order order = new Order(this.getToppings()); 
        order.makeOrder();
        this.orderList.add(order);
        return order.getOrderJson();
    }

    /**
     * Test Order to see if matchPizza() method
     * in SubmitButton works.
     */
    public String createFakeOrder() throws JsonProcessingException {
        Order order = new Order(this.getToppings()); 
        order.testOrder();
        this.orderList.add(order);
        return order.getOrderJson();
    }

    public List<String> getToppings() {
        return this.toppings;
    }

    public int getScore() {
        return this.finalScore;
    }

    public void removeOrder() {
        this.orderList.remove(0);
    }

    public void retryLevel() {
        this.orderList = new ArrayList<>();
        this.finalScore = 0;
    }

    public void changeScore(int score) {
        this.finalScore = score;
    }
}