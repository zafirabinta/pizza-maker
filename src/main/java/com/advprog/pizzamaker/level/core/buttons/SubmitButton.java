package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;
import com.advprog.pizzamaker.level.core.Order;
import com.advprog.pizzamaker.pizzadecorator.core.Pizza;

public class SubmitButton extends Button {
    private String name = "submit";

    public SubmitButton(Level level) {
        super(level);
    }

    @Override
    public int submit(int time) {
        int questScore = 0;
        if (!matchPizza()) {
            questScore = -50;
        } else {
            questScore = (100 + time * 10) * 10;
        }

        this.level.increaseScore(questScore);
        return questScore;
    }

    public void execute() {
        // do nothing
    }

    public int penalty() {
        return 0;
    }

    public String getName() {
        return this.name;
    }

    /**
     * To check every topping in pizza.
     */
    public boolean matchPizza() {
        Pizza pizza = this.level.getPizza();
        Order order = this.level.getFirstOrder();
        if (order.getTopping("pepperoni", pizza.getPepperoni())) {
            if (order.getTopping("mushroom", pizza.getMushroom())) {
                if (order.getTopping("cheese", pizza.getCheese())) {
                    return true;
                }
            }
        }

        return false;
    }
}