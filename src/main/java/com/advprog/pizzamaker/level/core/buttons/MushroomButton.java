package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public class MushroomButton extends ToppingButton {
    private String name = "mushroom";

    public MushroomButton(Level level) {
        super(level);
    }

    @Override
    public void execute() {
        this.level.getAddOn().addMushroom();
    }

    public String getName() {
        return this.name;
    }

    public int submit(int time) {
        return 0;
    }

    public int penalty() {
        return 0;
    }
}