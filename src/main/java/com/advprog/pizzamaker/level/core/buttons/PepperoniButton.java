package com.advprog.pizzamaker.level.core.buttons;

import com.advprog.pizzamaker.level.core.Level;

public class PepperoniButton extends ToppingButton {
    private String name = "pepperoni";

    public PepperoniButton(Level level) {
        super(level);
    }

    @Override
    public void execute() {
        this.level.getAddOn().addPepperoni();
    }

    public String getName() {
        return this.name;
    }

    public int submit(int time) {
        return 0;
    }

    public int penalty() {
        return 0;
    }
}