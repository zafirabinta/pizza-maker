$(document).ready(function () {
    loading();
})


function hideResult() {
    $("#result").hide();
}

function showResult() {
    $("#result").show();
}

function loading() {
    var players = "${topScorer}"
    var text = ''
    var textHead = '<tr>\n' +
        '<th>Rank</th>\n' +
        '<th>Level</th>\n' +
        '<th>Name</th>\n' +
        '<th>Score</th>\n' +
        '</tr>'
    $.ajax({
        url:'/topScore',
        type: 'GET',
        data: {
            'player':players
        }
    ,
    beforeSend: hideResult(),
    success: function (player) {
        $('#thead').append(
            textHead
        )
        showResult();
        }
    })

}